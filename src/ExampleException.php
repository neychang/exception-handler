<?php 

namespace ExampleException extends Exception 
{
    use Reportable;

    public function report()
    {
        $this->mail();
    }
}