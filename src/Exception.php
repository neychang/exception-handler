
<?php 

namespace Ney\Exception;

use Exception as BaseException;

abstract class Exception extends BaseException
{
    public function report();
    
    public function render();
}